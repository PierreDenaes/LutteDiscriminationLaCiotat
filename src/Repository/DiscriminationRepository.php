<?php

namespace App\Repository;

use App\Entity\Discrimination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Discrimination|null find($id, $lockMode = null, $lockVersion = null)
 * @method Discrimination|null findOneBy(array $criteria, array $orderBy = null)
 * @method Discrimination[]    findAll()
 * @method Discrimination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscriminationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Discrimination::class);
    }

    // /**
    //  * @return Discrimination[] Returns an array of Discrimination objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Discrimination
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
