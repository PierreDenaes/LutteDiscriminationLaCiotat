<?php
namespace App\Security;
use App\Entity\Admin as AppUser;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    //
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) return;

        // L’utilisateur n’est pas activé par l’administrateur
        if (!$user->getEnabled()) {
            throw new AccountDisabledException();
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
    }
}