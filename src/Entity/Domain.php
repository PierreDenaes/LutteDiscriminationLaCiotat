<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomainRepository")
 */
class Domain
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Discrimination", mappedBy="domain")
     */
    private $discriminations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Declaration", mappedBy="domain")
     */
    private $declarations;

    public function __construct()
    {
        $this->discriminations = new ArrayCollection();
        $this->declarations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Discrimination[]
     */
    public function getDiscriminations(): Collection
    {
        return $this->discriminations;
    }

    public function addDiscrimination(Discrimination $discrimination): self
    {
        if (!$this->discriminations->contains($discrimination)) {
            $this->discriminations[] = $discrimination;
            $discrimination->addDomain($this);
        }

        return $this;
    }

    public function removeDiscrimination(Discrimination $discrimination): self
    {
        if ($this->discriminations->contains($discrimination)) {
            $this->discriminations->removeElement($discrimination);
            $discrimination->removeDomain($this);
        }

        return $this;
    }

    /**
     * @return Collection|Declaration[]
     */
    public function getDeclarations(): Collection
    {
        return $this->declarations;
    }

    public function addDeclaration(Declaration $declaration): self
    {
        if (!$this->declarations->contains($declaration)) {
            $this->declarations[] = $declaration;
            $declaration->setDomain($this);
        }

        return $this;
    }

    public function removeDeclaration(Declaration $declaration): self
    {
        if ($this->declarations->contains($declaration)) {
            $this->declarations->removeElement($declaration);
            // set the owning side to null (unless already changed)
            if ($declaration->getDomain() === $this) {
                $declaration->setDomain(null);
            }
        }

        return $this;
    }
}
