<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StructureRepository")
 */
class Structure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $acronyme;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $schedule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $permanence;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Discrimination", inversedBy="structures")
     */
    private $hasDiscrimination;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referent", mappedBy="structures", orphanRemoval=true)
     */
    private $referents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keyword", inversedBy="structures")
     */
    private $keyword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Network", mappedBy="structure")
     */
    private $networks;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    public function __construct()
    {
        $this->hasDiscrimination = new ArrayCollection();
        $this->referents = new ArrayCollection();
        $this->keyword = new ArrayCollection();
        $this->networks = new ArrayCollection();
    }
    public function __toString(): string
    {
        return $this->name.''.$this->acronyme.''.$this->schedule.''.$this->permanence;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronyme(): ?string
    {
        return $this->acronyme;
    }

    public function setAcronyme(?string $acronyme): self
    {
        $this->acronyme = $acronyme;

        return $this;
    }

    public function getSchedule(): ?string
    {
        return $this->schedule;
    }

    public function setSchedule(string $schedule): self
    {
        $this->shcedule = $schedule;

        return $this;
    }

    public function getPermanence(): ?string
    {
        return $this->permanence;
    }

    public function setPermanence(string $permanence): self
    {
        $this->permanence = $permanence;

        return $this;
    }

    /**
     * @return Collection|Discrimination[]
     */
    public function getHasDiscrimination(): Collection
    {
        return $this->hasDiscrimination;
    }

    public function addHasDiscrimination(Discrimination $hasDiscrimination): self
    {
        if (!$this->hasDiscrimination->contains($hasDiscrimination)) {
            $this->hasDiscrimination[] = $hasDiscrimination;
        }

        return $this;
    }

    public function removeHasDiscrimination(Discrimination $hasDiscrimination): self
    {
        if ($this->hasDiscrimination->contains($hasDiscrimination)) {
            $this->hasDiscrimination->removeElement($hasDiscrimination);
        }

        return $this;
    }

    /**
     * @return Collection|Referent[]
     */
    public function getReferents(): Collection
    {
        return $this->referents;
    }

    public function addReferent(Referent $referent): self
    {
        if (!$this->referents->contains($referent)) {
            $this->referents[] = $referent;
            $referent->setStructures($this);
        }

        return $this;
    }

    public function removeReferent(Referent $referent): self
    {
        if ($this->referents->contains($referent)) {
            $this->referents->removeElement($referent);
            // set the owning side to null (unless already changed)
            if ($referent->getStructures() === $this) {
                $referent->setStructures(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Keyword[]
     */
    public function getKeyword(): Collection
    {
        return $this->keyword;
    }

    public function addKeyword(Keyword $keyword): self
    {
        if (!$this->keyword->contains($keyword)) {
            $this->keyword[] = $keyword;
        }

        return $this;
    }

    public function removeKeyword(Keyword $keyword): self
    {
        if ($this->keyword->contains($keyword)) {
            $this->keyword->removeElement($keyword);
        }

        return $this;
    }

    /**
     * @return Collection|Network[]
     */
    public function getNetworks(): Collection
    {
        return $this->networks;
    }

    public function addNetwork(Network $network): self
    {
        if (!$this->networks->contains($network)) {
            $this->networks[] = $network;
            $network->addStructure($this);
        }

        return $this;
    }

    public function removeNetwork(Network $network): self
    {
        if ($this->networks->contains($network)) {
            $this->networks->removeElement($network);
            $network->removeStructure($this);
        }

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
