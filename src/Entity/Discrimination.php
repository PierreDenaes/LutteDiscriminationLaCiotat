<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscriminationRepository")
 */
class Discrimination
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Declaration", mappedBy="discrimination", orphanRemoval=true)
     */
    private $declarations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Structure", mappedBy="hasDiscrimination")
     */
    private $structures;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keyword", inversedBy="discriminations")
     */
    private $keyword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Domain", inversedBy="discriminations")
     */
    private $domain;

    public function __construct()
    {
        $this->declarations = new ArrayCollection();
        $this->structures = new ArrayCollection();
        $this->keyword = new ArrayCollection();
        $this->domain = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->type;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Declaration[]
     */
    public function getDeclarations(): Collection
    {
        return $this->declarations;
    }

    public function addDeclaration(Declaration $declaration): self
    {
        if (!$this->declarations->contains($declaration)) {
            $this->declarations[] = $declaration;
            $declaration->setDiscrimination($this);
        }

        return $this;
    }

    public function removeDeclaration(Declaration $declaration): self
    {
        if ($this->declarations->contains($declaration)) {
            $this->declarations->removeElement($declaration);
            // set the owning side to null (unless already changed)
            if ($declaration->getDiscrimination() === $this) {
                $declaration->setDiscrimination(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getStructures(): Collection
    {
        return $this->structures;
    }

    public function addStructure(Structure $structure): self
    {
        if (!$this->structures->contains($structure)) {
            $this->structures[] = $structure;
            $structure->addHasDiscrimination($this);
        }

        return $this;
    }

    public function removeStructure(Structure $structure): self
    {
        if ($this->structures->contains($structure)) {
            $this->structures->removeElement($structure);
            $structure->removeHasDiscrimination($this);
        }

        return $this;
    }

    /**
     * @return Collection|Keyword[]
     */
    public function getKeyword(): Collection
    {
        return $this->keyword;
    }

    public function addKeyword(Keyword $keyword): self
    {
        if (!$this->keyword->contains($keyword)) {
            $this->keyword[] = $keyword;
        }

        return $this;
    }

    public function removeKeyword(Keyword $keyword): self
    {
        if ($this->keyword->contains($keyword)) {
            $this->keyword->removeElement($keyword);
        }

        return $this;
    }

    /**
     * @return Collection|Domain[]
     */
    public function getDomain(): Collection
    {
        return $this->domain;
    }

    public function addDomain(Domain $domain): self
    {
        if (!$this->domain->contains($domain)) {
            $this->domain[] = $domain;
        }

        return $this;
    }

    public function removeDomain(Domain $domain): self
    {
        if ($this->domain->contains($domain)) {
            $this->domain->removeElement($domain);
        }

        return $this;
    }

}
