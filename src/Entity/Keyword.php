<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KeywordRepository")
 */
class Keyword
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $keyword;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Structure", mappedBy="keyword")
     */
    private $structures;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Discrimination", mappedBy="keyword")
     */
    private $discriminations;

    public function __construct()
    {
        $this->structures = new ArrayCollection();
        $this->discriminations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->keyword;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getStructures(): Collection
    {
        return $this->structures;
    }

    public function addStructure(Structure $structure): self
    {
        if (!$this->structures->contains($structure)) {
            $this->structures[] = $structure;
            $structure->addKeyword($this);
        }

        return $this;
    }

    public function removeStructure(Structure $structure): self
    {
        if ($this->structures->contains($structure)) {
            $this->structures->removeElement($structure);
            $structure->removeKeyword($this);
        }

        return $this;
    }

    /**
     * @return Collection|Discrimination[]
     */
    public function getDiscriminations(): Collection
    {
        return $this->discriminations;
    }

    public function addDiscrimination(Discrimination $discrimination): self
    {
        if (!$this->discriminations->contains($discrimination)) {
            $this->discriminations[] = $discrimination;
            $discrimination->addKeyword($this);
        }

        return $this;
    }

    public function removeDiscrimination(Discrimination $discrimination): self
    {
        if ($this->discriminations->contains($discrimination)) {
            $this->discriminations->removeElement($discrimination);
            $discrimination->removeKeyword($this);
        }

        return $this;
    }
}
