<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeclarationRepository")
 */
class Declaration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Discrimination", inversedBy="declarations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $discrimination;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain", inversedBy="declarations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $domain;

    public function __toString(): string
    {
        return $this->name.''.$this->email.''.$this->content;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function __construct()
    {
        $this->discrimination = new ArrayCollection();
        $this->domain = new ArrayCollection();
    }

    public function getDiscrimination()
    {
        return $this->discrimination;
    }

    public function setDiscrimination(?Discrimination $discrimination): self
    {
        $this->discrimination = $discrimination;

        return $this;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }
}
