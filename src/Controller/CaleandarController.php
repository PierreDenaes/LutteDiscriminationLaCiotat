<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CaleandarController extends AbstractController
{
    /**
     * @Route("/caleandar", name="caleandar_action")
     * @param EventRepository $eventRepository
     * @return JsonResponse
     */
    public function caleandar(EventRepository $eventRepository)
    {
        $arrayCollection = $eventRepository->findAll();
        $events = array();

        // build json response
        foreach($arrayCollection as $item) {
            $events[] = array(

                'title' => $item->getTitle(),
                'date' => ['year' => $item->getBeginAt()->format("Y"),
                    'month' => $item->getBeginAt()->format("m"),
                    'day' => $item->getBeginAt()->format("d")],
                'time' => $item->getBeginAt()->format("H:i:s"),
                //'endTime' => $item->getEndAt()->format("H:i:s"),

            );
        }
        return new JsonResponse($events);
    }
}
