<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AccueilController extends AbstractController
{

    /**
     * @Route("/", name="accueil")
     * @param EventRepository $eventRepository
     * @return Response
     */
    public function index(EventRepository $eventRepository)
    {
        $events = $eventRepository->findAll();
        return $this->render('accueil/accueil.html.twig', [
            'events' => $events,
        ]);
    }
}

