<?php

namespace App\Controller;


use App\Form\FormContactType;

use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_action")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return Response
     */
    public function contact(Request $request, Swift_Mailer $mailer)
    {

        $form = $this->createForm(FormContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->addFlash('success', 'Votre message est envoyé');
            $contact = $form->getData();
            $message = (new Swift_Message())
                ->setSubject($contact['subject'])
                ->setFrom([$contact['mail'] => $contact['name']])
                ->setTo('photostudio13000@gmail.com')
                ->setBody(
                    $contact['content'],
                    'text/plain'
                );
            $mailer->send($message);

            return $this->redirectToRoute('contact_action');

        }

        return $this->render('contact/contact.html.twig', [
            'form' => $form->createView(),
        ]);
//
    }

}
