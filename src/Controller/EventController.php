<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class EventController extends AbstractController
{
    /**
     * @Route("/event", name="event_action")
     * @param Environment $twig
     * @param EventRepository $eventRepository
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function index(Environment $twig, EventRepository $eventRepository)
    {
        return new Response($twig->render('event/index.html.twig', ['events' => $eventRepository->findAll(),]));
    }

    /**
     * @Route("/event/{id}", name="event")
     * @param Environment $twig
     * @param Event $event
     * @param EventRepository $eventRepository
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function show(Environment $twig, Event $event, EventRepository $eventRepository)
    {
        return new Response($twig->render('event/show.html.twig', [
            'event' => $event,
            'events' => $eventRepository->findBy(['title' => $event], ['beginAt' => 'DESC']),
        ]));
    }
}
