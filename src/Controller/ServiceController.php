<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Repository\StructureRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ServiceController extends InfosController
{
    /**
     * @Route("/structure", name="service_action")
     * @param Request $request
     * @param StructureRepository $structureRepository
     * @return JsonResponse
     */

    public function load(Request $request, StructureRepository $structureRepository)
    {
        $arrayCollection = $structureRepository->findAll();
        $response = $arrayCollection;
        foreach($arrayCollection as $item) {
            $response []= [
                'label' => $item->getName(),
                'value'  => $item->getId(),
                'phone' => $item->getPhone(),
                'email' => $item->getMail()
            ];

        }
        return new JsonResponse($response);
    }

}
