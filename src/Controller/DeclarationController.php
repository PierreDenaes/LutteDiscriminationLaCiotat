<?php

namespace App\Controller;
use App\Entity\Declaration;
use App\Form\DeclarationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeclarationController extends AbstractController
{
    /**
     * @Route("/declaration", name="declaration_action")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request)
    {
        $declaration = new Declaration();
        $declaration->setName('name');
        $declaration->setEmail('email');
        $declaration->setContent('content');
        $declaration->getDiscrimination();
        $declaration->getDomain();

        $form = $this->createForm(DeclarationFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Votre message est envoyé');
            // $form->getData() holds the submitted values
            // but, the original `$declaration` variable has also been updated
            $declaration = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Declaration is a Doctrine entity, save it!
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($declaration);
             $entityManager->flush();

            return $this->redirectToRoute('declaration_action');
        }
        return $this->render('declaration/declaration.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
