
$( function() {
    function log( response ) {
        $( "#result" ).html( response );
    }
    function split( val ) {
        return val.split( /,\s*/ );
    }
    function extractLast( term ) {
        return split( term ).pop();
    }

    $( "#autocomplete" ).autocomplete({
        minLength: 2,
        source: function( request, response,) {
            // Fetch data
            $.ajax({
                url: "/structure",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function( data ) {
                    response( $.ui.autocomplete.filter(data, extractLast( request.term )));

                }

            });
        },
        select: function (event, ui) {
            // Set selection
            $('#autocomplete').val(ui.item.label);
            log(ui.item.label+' <br> mail: '+ ui.item.email +' <br> phone: '+ ui.item.phone); // display the selected text

            return false;
        },
    });
});