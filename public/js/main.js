// fonction de switch de classe ouverte fermée
let openMenu = document.querySelector("#openMenu");
openMenu.onclick = function () {
    const burger = document.querySelector(".svgBtnMenu");
    burger.classList.toggle("svgBtnMenu-open");
    const menuB = document.querySelector(".yellowMenu");
    menuB.classList.toggle("yellowMenu-open");
    const navigate = document.querySelector(".navigate");
    navigate.classList.toggle("navigate-open");
    const iconbtn = document.querySelector(".icon");
    iconbtn.classList.toggle("icon-open");
}
// fonction d'opacité du menu au scroll
let prevScrollpos = window.scrollY;
window.onscroll = function() {
    let currentScrollPos = window.scrollY;
    if (prevScrollpos > currentScrollPos) {
        document.querySelector(".yellowMenu").style.opacity = "1";
        document.querySelector(".blueBottom").style.opacity = "1";
    } else {
        document.querySelector(".yellowMenu").style.opacity = "0.1";
        document.querySelector(".blueBottom").style.opacity = "0";
    }
    prevScrollpos = currentScrollPos;
};