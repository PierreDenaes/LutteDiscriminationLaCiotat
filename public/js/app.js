
$(document).ready( function() {

    console.log("JQuery ready!!!!!!.");
    let events = [];
    let settings = {
        Color: 'gray',
        LinkColor: 'hsl(198, 64%, 41%)',
        NavShow: true,
        NavVertical: false,
        NavLocation: '',
        DateTimeShow: true,
        DateTimeFormat: 'mmm, yyyy',
        DatetimeLocation: '',
        EventClick: '',
        EventTargetWholeDay: false,
        DisabledDays: [],
    };
    
    let jxhr = $.ajax({
        type: 'GET',
        url: '/caleandar',
        DataType: 'json',
        success: function(){
      
            let eventArrays = JSON.parse(jxhr.responseText);
            for(idx = 0; idx < eventArrays.length; idx++)
            {   
                let obj = {};
                obj.Date  = new Date( 
                                    eventArrays[idx].date.year, 
                                    eventArrays[idx].date.month-1,  // la date commence à zero
                                    eventArrays[idx].date.day
                                    );
                obj.Title = eventArrays[idx].title+' at '+eventArrays[idx].time+'h.';
                obj.Link  = '/event/'+[idx+1];  //TODO: transform to array many event in one day
                events.push(obj);                                    
            } 
            console.log("ajax success.");
            console.log(events);
            console.log(events);
        },
        error: function() { console.log("error occurred.") },
        complete: function() {
            
            let element = document.getElementById('caleandar');
            caleandar(element, events, settings);

            console.log("Cleandar element: "+element+"\n"+
            "Events global: "+events+"\n"+
            "Cleandar settings: "+settings);
            console.log("Caleandar created.");
            console.log("ajax complete.") }
    });

return false;
});