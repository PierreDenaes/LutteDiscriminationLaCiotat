# Projet plateforme Anti-discrimination

![image anti-discrimination](https://www.coe.int/documents/18550040/23907883/image_Support+to+Ombudsperson+and+anti-discrimination+institutions+National+Human+Rights+Institutions-mod.jpg/a7150314-170f-4413-ad49-dbbd134bc9bb?t=1486054757000)

> #### Stack technique
> * Serveur local Apache
> * Serveur local MySQL
> * PHP 7.3 minimum
> * Symfony 5 
> * Composer
> * Yarn
> * Node
> * Webpack-encore-bundle
> * easyAdminBundle
> * VichUploaderBundles
> * Jquery-ui-autocomplete
> * caleandar.js
> * Sass-loader
> * laminas en remplacement de zend

### Résumé

Lors de mon stage en entreprise, j’ai eu l’occasion de travailler dans une agence Web fictive répondant
à deux contrats réels. L’un des contrats était la création d’un POC à destination de la lutte contre les
discriminations. Cette plateforme devait permettre la mise en relation des associations avec les
victimes, le regroupement des associations sous une même structure pour assurer un meilleur réseau
ainsi qu’un outil d’alerte à usage des victimes.

Pour ce projet j’ai en premier lieu imaginé et conceptualisé la base de données sur papier en posant la
structure du mcd. J’ai ensuite procédé à la réalisation du mld sur workbench pour commencer à
enregistrer celle-ci dans le framework Symfony lui-même utilisé pour créer mon application.

Une fois la base de données créée via Symfony et doctrine Orm, j’ai installé divers bundles afin de
parfaire notre application. En premier j’ai installé WebpackEncoreBundle dans le but de pouvoir utiliser
un pre-processeur Sass dans le dévellopement de mon applications Symfony.

Ensuite j’ai installé EasyAdminbundle pour mettre une interface BackOffice opérationnelle dans la
gestion du Crud sur les tables de ma base de données. J’ai créé des contrôleurs Symfony pour
l’affichage des données du site dans mes templates Twig selon les conditions implantées. Nous avons
override le bundle EasyAdmin pour lui permetttre d’encoder un password au moment de la création ou
de la modification d’un administrateur.

J’ai installé un dernier bundle VichUploader pour gérer le téléchargement dans mon application et ma
base de données de fichiers type image (multi format) et pdf.
J’ai implémenté un composant de sécurité Symfony afin de permettre la vérification d’une condition
booléen « isActive » de mes comptes d’administration avant connexion.
J’ai élaboré une gestion de contenu de type création événementiel en backOffice avec système de
publication et de partage.
J’ai créé un formulaire de déclaration de discrimination a usage des victimes relié à la base de données.
J’ai mis en place un autre formulaire de contact fonctionnant en mail direct sans enregistrement en
base de données via SwiftMailer.

Au moment du déploiement du site via une connexion ssh au serveur, j’ai dû installer Symfony/apachepack.

Le reste des étapes concernent le travail du développement FrontEnd avec la mise en place d’un outil
de recherche type annuaire avec une fonction d’auto-complète, affichant les numéros des associations
présentes en base de données.